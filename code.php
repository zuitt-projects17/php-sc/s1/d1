<?php


/* 

	[SECTION] Comments
	Comments are part of the codfe thaty get ignored by the language.


	There are two types of comments:

	- Single comments denoted by //
	- Multi-line comments denoted by a slash and an asterisk closed by an asterisk and a slash.



	[SECTION] Variables

		- are used to contain data
		- are a named location for the stored value
		- are defined in PHP are using the dollar ($) notation before the name of the variable


*/

		$name = "John Smith";
		$email = "johnsmith@gmail.com";

		// Variable reassignment
		$name = "John Johnson";



/*
[SECTION] Constants

	Constants used to hold data that are meant to be read-only (value cannot be changed)
	Constants are defined using the define() function

*/


	define("PI", 3.1416);


/*
	Syntax:
	define(var_name, var_value)

	By convention, constant names are in ALL CAPS



	[SECTION] Data Types

	Strings:
*/

	$state = "New York";
	$country = 'United States of America';
	$address = $state . "," . $country; //concatenation via . sign
	$address = "$state, $country"; //concatenation via double quotes


	// Integers
	$age = 31;
	$headcount = 26;


	// Floats
	$grade = 98.2;
	$distanceInKilometers = 1342.12;


	// Boolean
	$hasTraveledAbroad = false;
	$hasSymptoms = true;


	// Null
	$spouse = null;


	// Objects
	$person = (object)[

		'fullName' => 'John Smith',
		'isMarried' => false,
		'age' => 35,
		'address' => (object)[

			'state' => 'New York',
			'country' => 'United States of America'

		]

	];


	// Arrays
	$grades = array(98.7, 92.1, 90.2, 94.6);



	/*
		[SECTION] Operators

		Assignment Operator
			- used to assign values to a variables
	*/


	$x = 1234;
	$y = 1235;


	$isLegalAge = true;
	$isRegistered = false;





/*

[SECTION] Functions
	
	- Functions are used to make reusable code that can be called/invoked

*/


	function getFullName($firstName, $middleInitial, $lastName){

		return "$lastName, $firstName $middleInitial";

	}


/*
[SECTION] Selection COntrol Structures/Conditional Statements
	- Selection Control Structures are used to makes code execution dynamic according to predefinedconditions.

*/


function getTyphoonIntensity($windSpeed){

	if($windSpeed < 30){
		return 'Not a typohoon yet';

	}else if($windSpeed <= 61){
		return 'Tropical depression detected';

	}else if($windSpeed >= 62 && $windSpeed <= 88){
		return 'Tropical storm detected';

	}else if($windSpeed >= 89 && $windSpeed <= 117){
		return 'Sever tropical storm detected';

	}else{
		return 'Typhoon detected';
	}
}


// If-Elseif-Else statements do not need to be wrapped ina function


// Ternary Operator

function isUnderAge ($age){
	return ($age < 18) ? true : false;

}

// (condition) ? statement1 : statement2;
// If condition is true, execute statement 1, If not, execute statement 2.
// Also does not need to be in a function

























