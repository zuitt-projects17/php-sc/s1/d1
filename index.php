

<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S1: PHP Basics and Selection Control Structures</title>
</head>
<body>

	<h1>Echoing Values</h1>
	<!--  Concatination  -->
	<p><?php echo "Good Day" . $name . "! Your given email is " . $email . ".";  ?></p>

	<!--  Double quotes  -->
	<!--  When embedding variables inside of strings, the strict usage of " " around your string is REQUIRED  -->
	<p><?php echo "Good Day $name! Your given email is $email."; ?></p>

	<p><?php echo PI; ?></p>
	<p><?php echo "Your address is $address" ?></p>

	<!--  Normal echoing of boolean and null values will not be visible in the webpage.  -->
	<p><?php echo $hasTraveledAbroad; ?></p>
	<p><?php echo $spouse; ?></p>

	<!--  In order to print them out, we can use var_dump()  -->
	<p><?php echo var_dump ($spouse) ?></p>

	<!--  To see value's data type, we can use gettype()  -->
	<p><?php echo gettype($hasTraveledAbroad); ?></p>

	<!--  To output the value of an object properly, the arrow noation can be used  -->
	<p><?php echo $person ->address->state; ?></p>

	<!-- To output an array element, we can use the usual square brackets  -->
	<p><?php echo $grades[3]; ?></p>

"
	<h1>Operators</h1>


	<h2>Arithmetic Operators</h2>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Quotient: <?php echo $x / $y; ?></p>


	<h2>Equality Operators</h2>
	<p>Loose Equality: <?php echo var_dump ($x == '1234'); ?></p>
	<p>Strict Equality: <?php echo var_dump ($x === '1234'); ?></p>
	<p>Loose Inequality: <?php echo var_dump ($x != '1234'); ?></p>
	<p>Strict Inequality: <?php echo var_dump ($x !== '1234'); ?></p>


	<h2>Comparison Operators</h2>
	<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater: <?php echo var_dump($x > $y); ?></p>
	<p>Is Lesser or Equal: <?php echo var_dump($x <= $y); ?></p>
	<p>Is Greater or Equal: <?php echo var_dump($x >= $y); ?></p>


	<h2>Logical Operators</h2>
	<p>Are All Requirements Met? <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are Some Requirements Met? <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Are Some Requirements NOT Met? <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>



	<h1>Functions</h1>
	<p>Full Name: <?php echo getFullName("John", "Dane", "Smith");?></p>



	<h1>Selection Control Structures</h1>
	
	<h2>If-Elseif-Else</h2>
	<p><?php echo getTyphoonIntensity(12); ?></p>


	<h2>Ternary Sample</h2>
	<p><?php echo var_dump(isUnderAge(78)); ?></p>
	<p><?php echo var_dump(isUnderAge(17)); ?></p>



</body>
</html>















